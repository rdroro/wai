# WAI - World After Internet

World After Internet - WAI - Un nom cool reste à trouver

Projet d'une boîte "fin du monde" aka « plus d'accès  à internet » contenant de façon autonome les connaissances pour continuer à vivre.
Le système doit être autonome et fonctionner avec peu d'énergie - batterie ou panneau solaire et doit être résilient.

# Pour contribuer

Dans la partie **Issues**, ajouter vos idées des connaissances qui vous semblent
importantes de stocker/sauvegarder si jamais Internet était coupé ou si nous n'avions
pas assez d'électricité pour alimenter la box + le téléphone.

Pour créer les idées, [un guide est disponible](https://gitlab.com/rdroro/wai/-/issues/4)

Danse cette première phase, l'idée est simplement de rescencer les connaissances
et les usages que l'on trouve important à stocker sans jugement (ne pas hésiter à demander autour
de vous à différentes tranche d'âge par exemple).




# Processus de création

1.  Récolte des idées - Ajout des idées en mettant le label **Idea**
2.  Discussions - Lecture de toutes les idées et échages pour approfondir les idées
3.  Priorisation - Vote pour prioriser les idées à mettre dans la box (le podium passe à l'étape d'après)
4.  Ajout des idées dans la box - Le podium est intégré dans la box

Itération de 1. à 4. de façon continue